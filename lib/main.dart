
import 'package:flutter/material.dart';
void main() => runApp(MyApp());
/// This Widget is the main application widget.
class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter App',
      home: MyStatefulWidget(),
    );
  }
}
class MyStatefulWidget extends StatefulWidget {
  MyStatefulWidget({Key key}) : super(key: key);
  @override
  _MyStatefulWidgetState createState() => _MyStatefulWidgetState();

  static Container _buildBodyText(String textPath) {
    return Container(
      padding: const EdgeInsets.all(12),
      child: Text(textPath)
    );
  }
  static Container _buildTextRow(String boldText, String underText) {
    return Container(
        padding: const EdgeInsets.all(32),
        child: Row(
          children: [
            Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      padding: const EdgeInsets.only(bottom: 8),
                      child: Text(boldText, style: TextStyle(fontWeight: FontWeight.bold)),
                    ),
                    Text(underText, style: TextStyle(color: Colors.grey[500]))
                  ],
                )
            )
          ],
        )
    );
  }

  static Container _buildInfoPages(String imagePath, String boldText, String greyText, String bodyText) {
    return Container(
        padding: const EdgeInsets.all(32),
        child: Column (
        children: [
          Image.asset(imagePath, width: 600, height:240),
            _buildTextRow(boldText, greyText),
            _buildBodyText(bodyText),
      ])
    );
  }

}


class _MyStatefulWidgetState extends State<MyStatefulWidget> {
  int _selectedIndex = 0;

  static const TextStyle optionStyle = TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  List<Widget> _widgetOptions = <Widget>[
    MyStatefulWidget._buildTextRow('Click the different icons to learn about my favorite color and food',''),
    Container(
        child: MyStatefulWidget._buildInfoPages('assets/images/red.png','Red is my favorite color', 'About the color red:','Red is the color at the end of the visible spectrum of light, next to orange and opposite violet. It has a dominant wavelength of approximately 625–740 nanometres.[1] It is a primary color in the RGB color model and the CMYK color model, and is the complementary color of cyan. Reds range from the brilliant yellow-tinged scarlet and vermillion to bluish-red crimson, and vary in shade from the pale red pink to the dark red burgundy.'),
    ),
    Container(
      child: MyStatefulWidget._buildInfoPages('assets/images/dog.jpg','Corgis are my favorite animal', 'About the Pembroke Welsh Corgi:','At 10 to 12 inches at the shoulder and 27 to 30 pounds, a well-built male Pembroke presents a big dog in a small package. Short but powerful legs, muscular thighs, and a deep chest equip him for a hard day’s work. Built long and low, Pembrokes are surprisingly quick and agile. They can be red, sable, fawn, and black and tan, with or without white markings.'),
    ),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Layout Homework'),
      ),
      body: ListView(
        children: [_widgetOptions.elementAt(_selectedIndex)]
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text('Home'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.palette),
            title: Text('Color'),
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.bug_report),
            title: Text('Animal'),
          ),
        ],
        currentIndex: _selectedIndex,
        selectedItemColor: Colors.amber[800],
        onTap: _onItemTapped,
      ),
    );
  }
}
